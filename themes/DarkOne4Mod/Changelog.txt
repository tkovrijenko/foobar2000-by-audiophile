Version 1.0
build20161003
* fixed: resize issues in ELPlaylist
* fixed: resize issues for Quicksearch toolbar
* added: configuration files for easier installation (read the "Install Instructions.txt" file!)
build20160929
* Initial release of modified design
Changes since DarkOne v4.0:
* changed: renamed project to DarkOne4Mod
* changed: made the whole config resizable to keep aspect ratio on every resolution up to 4K
* changed: now a panel can be added below Waveform seekbar, if resolution is not 16:9/10 - see Manual4mod.pdf, chapter 2.3.3
* fixed: small issue in Cover Panel when "Auto Cycle Image" is enabled
* fixed: "Various" picture in ELPlaylist popup window that was always shown when ALBUM ARTIST TAG is set
* fixed: Display colour can be set through context menu for display and Analog VU Meter
* fixed: optimized scripts
* removed: track title below Waveform seekbar
* removed: WSH Panel Mod
* removed: outdated Peakmeter Spectrum component
* removed: obsolete Biography View Panel
* added: Analog VU Meter and skins for different resolutions (optional - see Manual4Mod.pdf)
* added: JScript Panel
* updated: component package