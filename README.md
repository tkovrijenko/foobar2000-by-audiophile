# foobar2000 by Audiophile

Author: Taras Kovryzhenko
Home page: https://audiophilesoft.com/load/audiophilesoftware/foobar2000_by_audiophile/3-1-0-56

A build of the foobar2000 player from the creator of the [Audiophile's Software](https://audiophilesoft.com) project. It includes a wide array of pre-installed plugins, encoders, utilities, etc. The player offers two interface options: the classic Default UI with comprehensive sound monitoring and the Columns UI with the DarkOneMod theme.

***

Збірка плеєра foobar2000 від автора проєкту [Audiophile's Software](https://audiophilesoft.com).
Містить велику кількість встановлених плагінів, кодерів, утиліт тощо. Має два варіанти інтерфейсу: класичний Default UI з повним моніторингом звуку і Columns UI з темою DarkOneMod.

